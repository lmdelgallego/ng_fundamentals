import { Component } from '@angular/core';

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'event-app',
  template: `
    <nav-bar></nav-bar>
    <events-list></events-list>
  `
})
export class EventAppComponent {
  title = 'ng-fundamentals';
}
